# Search Youtube

Get results in JSON from Youtube search.

## Requirements

- PHP 7.4
- Composer
- Docker
- Docker Compose

## Installation

```bash
$ cd search-youtube
$ composer install
```

## Usage

```bash
$ docker-compose up -d
$ php -S localhost:8081 -t public public/index.php
```

That done, the endpoint `http://localhost:8081/api/youtube?search=text` will be available.

Example:

Request: `http://localhost:8081/api/youtube?search=laravel 8 released`

Response:

```
{
  "data": [
    {
      "published_at": "há 4 dias",
      "id": "ahlbcrVSZgM",
      "title": "Laravel 8 - What's new features & step by step guide - Models and Factories",
      "description": "<b>Laravel 8</b> has just <b>released</b> and in this video we will see what are the new features available with <b>Laravel 8</b>, a step by step guide ...",
      "thumbnail": "https://i.ytimg.com/vi/ahlbcrVSZgM/hqdefault.jpg?sqp=-oaymwEjCNACELwBSFryq4qpAxUIARUAAAAAGAElAADIQj0AgKJDeAE=&rs=AOn4CLC_NX7Ppc3tkJV-37ZWJCOJ0aIu3g",
      "extra": {
        "url": "https://www.youtube.com/watch?v=ahlbcrVSZgM",
        "duration": "17:30",
        "views": "1.180 visualizações",
        "channel_url": "https://www.youtube.com/channel/UC4gijXR8cM4gmEt9Olse-TQ",
        "username": "Amitav Roy"
      }
    },
    {
      "published_at": "há 1 semana",
      "id": "X7gyErx8_fs",
      "title": "Laravel 8 - new feature | what's new in laravel 8",
      "description": "in this <b>laravel 8</b> video tutorial we learn what is new features in <b>laravel 8</b> and what's new in <b>laravel 8</b> feature in english language.",
      "thumbnail": "https://i.ytimg.com/vi/X7gyErx8_fs/hqdefault.jpg?sqp=-oaymwEjCNACELwBSFryq4qpAxUIARUAAAAAGAElAADIQj0AgKJDeAE=&rs=AOn4CLBw9GKz0VOFG6oN77S0rhQKVs4Lsw",
      "extra": {
        "url": "https://www.youtube.com/watch?v=X7gyErx8_fs",
        "duration": "14:15",
        "views": "3.245 visualizações",
        "channel_url": "https://www.youtube.com/channel/UCvHX2bCZG2m9ddUhwxudKYA",
        "username": "php step by step"
      }
    },
    {
      "published_at": "Transmitido há 2 semanas",
      "id": "30S8XIgz18s",
      "title": "Novidades Laravel 8 - Petiscando #016",
      "description": "Fala Artesãos, suave? Hoje vamos bater um papo sobre as novidades do <b>Laravel 8</b> que está com <b>release</b> date pro dia ...",
      "thumbnail": "https://i.ytimg.com/vi/30S8XIgz18s/hqdefault.jpg?sqp=-oaymwEjCNACELwBSFryq4qpAxUIARUAAAAAGAElAADIQj0AgKJDeAE=&rs=AOn4CLDLCzuFL7_VIGNfGvKc4dQgtBEWcw",
      "extra": {
        "url": "https://www.youtube.com/watch?v=30S8XIgz18s",
        "duration": "1:36:17",
        "views": "637 visualizações",
        "channel_url": "https://www.youtube.com/channel/UCtz8hxpbicBe8REEdEtAYWA",
        "username": "Beer and Code"
      }
    },
    {
      "published_at": "há 1 dia",
      "id": "2f4R8yJQV9k",
      "title": "06 - Qué es Laravel Jetstream - Novedades de Laravel 8",
      "description": "Utiliza el cupón STAYHOME para un 40% de descuento en todos los planes https://aprendible.com/planes.",
      "thumbnail": "https://i.ytimg.com/vi/2f4R8yJQV9k/hqdefault.jpg?sqp=-oaymwEjCNACELwBSFryq4qpAxUIARUAAAAAGAElAADIQj0AgKJDeAE=&rs=AOn4CLBBfZTML9nnBIfvS5-TOD3wot1akw",
      "extra": {
        "url": "https://www.youtube.com/watch?v=2f4R8yJQV9k",
        "duration": "9:02",
        "views": "1.360 visualizações",
        "channel_url": "https://www.youtube.com/channel/UC-R0zZjpkeoLHPxVTHolVHw",
        "username": "Aprendible"
      }
    },
    {
      "published_at": "há 4 dias",
      "id": "0z0qtt7Kc_I",
      "title": "Laravel 8 y JetStream ¡¡¡BRUTAL!!!",
      "description": "<b>Laravel 8</b> fue publicado el día 08/09/2020 y trae muchas novedades, aquí vemos algunas de ellas y una nueva y poderosa ...",
      "thumbnail": "https://i.ytimg.com/vi/0z0qtt7Kc_I/hqdefault.jpg?sqp=-oaymwEjCNACELwBSFryq4qpAxUIARUAAAAAGAElAADIQj0AgKJDeAE=&rs=AOn4CLDCCpxQJuJ89ajc_t12y0bGzEJhYw",
      "extra": {
        "url": "https://www.youtube.com/watch?v=0z0qtt7Kc_I",
        "duration": "7:59",
        "views": "1.281 visualizações",
        "channel_url": "https://www.youtube.com/channel/UCylETmSOiHASVVfpxQY8KVA",
        "username": "CursosDesarrolloWeb"
      }
    },
    {
      "published_at": "há 5 dias",
      "id": "BZFavPxP2xc",
      "title": "Laravel 8 JetStream Review - Authentication Scaffolding",
      "description": "Review of <b>Laravel 8</b> JetStream and all its features like 2FA, Team management, Browser Sessions, and much more. Check out ...",
      "thumbnail": "https://i.ytimg.com/vi/BZFavPxP2xc/hqdefault.jpg?sqp=-oaymwEjCNACELwBSFryq4qpAxUIARUAAAAAGAElAADIQj0AgKJDeAE=&rs=AOn4CLCqXcTCP4px4pQFCLQ2e_UcYj7d_g",
      "extra": {
        "url": "https://www.youtube.com/watch?v=BZFavPxP2xc",
        "duration": "49:29",
        "views": "13.925 visualizações",
        "channel_url": "https://www.youtube.com/channel/UC_hG9fglfmShkwex1KVydHA",
        "username": "Bitfumes"
      }
    },
    {
      "published_at": "há 1 semana",
      "id": "Q5S5_UjFxho",
      "title": "New features in Laravel 8",
      "description": "In this video, we will learn about <b>Laravel 8</b> is going to feature some nice new additions including an improved maintenance mode, ...",
      "thumbnail": "https://i.ytimg.com/vi/Q5S5_UjFxho/hqdefault.jpg?sqp=-oaymwEjCNACELwBSFryq4qpAxUIARUAAAAAGAElAADIQj0AgKJDeAE=&rs=AOn4CLA4hgRJkwlUvfDQF-0dKIu86NdrhQ",
      "extra": {
        "url": "https://www.youtube.com/watch?v=Q5S5_UjFxho",
        "duration": "10:54",
        "views": "1.335 visualizações",
        "channel_url": "https://www.youtube.com/user/siddharthshukla089",
        "username": "Real programmer"
      }
    },
    {
      "published_at": "há 5 dias",
      "id": "r0C9oJ4v_yI",
      "title": "Laravel 8 tutorial - update laravel 7 to 8",
      "description": "in this video we learn about <b>laravel 8</b> how to upgrade <b>laravel</b> 7 to <b>8</b> latest version . this video is made by anil Sidhu in english ...",
      "thumbnail": "https://i.ytimg.com/vi/r0C9oJ4v_yI/hqdefault.jpg?sqp=-oaymwEjCNACELwBSFryq4qpAxUIARUAAAAAGAElAADIQj0AgKJDeAE=&rs=AOn4CLAAcDK8FsSJr-IeyCPxRajCc6U9Cg",
      "extra": {
        "url": "https://www.youtube.com/watch?v=r0C9oJ4v_yI",
        "duration": "8:16",
        "views": "654 visualizações",
        "channel_url": "https://www.youtube.com/channel/UCvHX2bCZG2m9ddUhwxudKYA",
        "username": "php step by step"
      }
    },
    {
      "published_at": "há 4 dias",
      "id": "2XeVVHdUWMg",
      "title": "Laravel 8 Tutorial - Authentication",
      "description": "In this video I will talk about Authentication. TOPIC DISCUSSED: Authentication System Generate Auth Install <b>laravel</b>/jetstream ...",
      "thumbnail": "https://i.ytimg.com/vi/2XeVVHdUWMg/hqdefault.jpg?sqp=-oaymwEjCNACELwBSFryq4qpAxUIARUAAAAAGAElAADIQj0AgKJDeAE=&rs=AOn4CLDTJ0rOg8UwCNC3jf9cDf6Vy-gkZw",
      "extra": {
        "url": "https://www.youtube.com/watch?v=2XeVVHdUWMg",
        "duration": "9:50",
        "views": "1.333 visualizações",
        "channel_url": "https://www.youtube.com/channel/UCjE5x2IWoUxzY9yfQepL89g",
        "username": "Surfside Media"
      }
    },
    {
      "published_at": "há 2 dias",
      "id": "79fjVrH7Ph8",
      "title": "Laravel 8 tutorial # View",
      "description": "in this <b>laravel 8</b> video tutorial, we learn what is view and how to view in <b>laravel 8</b> project. this video is made by anil Sidhu in the ...",
      "thumbnail": "https://i.ytimg.com/vi/79fjVrH7Ph8/hqdefault.jpg?sqp=-oaymwEjCNACELwBSFryq4qpAxUIARUAAAAAGAElAADIQj0AgKJDeAE=&rs=AOn4CLDHwU3pPa-9_Xf8l4oDcf9oIoFKKw",
      "extra": {
        "url": "https://www.youtube.com/watch?v=79fjVrH7Ph8",
        "duration": "11:06",
        "views": "233 visualizações",
        "channel_url": "https://www.youtube.com/channel/UCvHX2bCZG2m9ddUhwxudKYA",
        "username": "php step by step"
      }
    }
  ]
}
```

