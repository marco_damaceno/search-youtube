<?php

use Slim\Factory\AppFactory;
use App\Error\ErrorHandler;
use App\Error\ErrorRenderer;

require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();

$app->addRoutingMiddleware();

$errorHandler = new ErrorHandler($app->getCallableResolver(), $app->getResponseFactory());
$errorMiddleware = $app->addErrorMiddleware(true, true, true);
$errorMiddleware->setDefaultErrorHandler($errorHandler);
$errorHandler->registerErrorRenderer('application/json', ErrorRenderer::class);
$errorHandler->forceContentType('application/json');

$entrypoint = require __DIR__ . '/../app/Components/entrypoint.php';
$entrypoint($app);

$app->run();
