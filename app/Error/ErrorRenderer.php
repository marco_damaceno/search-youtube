<?php

namespace App\Error;

use Slim\Interfaces\ErrorRendererInterface;

class ErrorRenderer implements ErrorRendererInterface
{
    public function __invoke(\Throwable $ex, bool $displayErrorDetails): string
    {
        $error = [
            'message' => $ex->getMessage(),
            'code' => $ex->getCode(),
        ];

        if (method_exists($ex, 'getDetails')) {
            $error['details'] = $ex->getDetails();
        }

        return json_encode([
            'error' => $error,
        ]);
    }
}
