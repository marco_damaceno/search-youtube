<?php

namespace App\Error;

use Slim\Handlers\ErrorHandler as SlimErrorHandler;

class ErrorHandler extends SlimErrorHandler
{
    protected function logError(string $error): void
    {
    }
}
