<?php

return function ($app) {
    $searchEntrypoint = require __DIR__ . '/Search/entrypoint.php';

    $searchEntrypoint($app);
};
