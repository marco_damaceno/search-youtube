<?php

return function ($app) {
    $searchController = require __DIR__ . '/controller.php';
    $app->get('/api/youtube', $searchController['index']);
};
