<?php

namespace App\Components\Search;

use App\Components\Search\Exception\NotFoundException;
use App\Components\Search\Exception\InputValidationException;

class Service
{
    private static $url = 'http://localhost:8080/api/search?q=';

    public static function handle($search)
    {
        if (empty($search)) {
            throw new InputValidationException(
                'Parameter "search" is not permitted to be empty',
                ['search']
            );
        }

        $ch = curl_init(self::$url . str_replace(' ', '+', $search));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);

        if ($info['http_code'] === 404) {
            throw new NotFoundException('Source not found');
        }

        $data = json_decode($response);

        if (is_null($data) || is_null($data->results)) {
            throw new NotFoundException('Results not found');
        }
        $result = array_slice($data->results, 0, 10);

        return array_filter($result, function ($el) {
            return !is_null($el->video);
        });
    }
}
