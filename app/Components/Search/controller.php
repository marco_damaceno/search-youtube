<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Components\Search\Service as SearchService;
use App\Components\Search\Presenter as SearchPresenter;

return [
    'index' => function (Request $request, Response $response) {
        $queries = $request->getQueryParams();

        $data = SearchService::handle($queries['search']);

        $payload = json_encode(['data' => SearchPresenter::resolve($data)]);

        $response->getBody()->write($payload);

        return $response->withHeader('Content-Type', 'application/json');
    },
];
