<?php

namespace App\Components\Search;

class Presenter
{
    public static function resolve(array $data = []) : array
    {
        $result = [];

        foreach ($data as $el) {
            array_push($result, [
                'published_at' => $el->video->upload_date,
                'id' => $el->video->id,
                'title' => $el->video->title,
                'description' => $el->video->snippet,
                'thumbnail' => $el->video->thumbnail_src,
                'extra' => [
                    'url' => $el->video->url,
                    'duration' => $el->video->duration,
                    'views' => $el->video->views,
                    'channel_url' => $el->uploader->url,
                    'username' => $el->uploader->username,
                ],
            ]);
        }

        return $result;
    }
}
