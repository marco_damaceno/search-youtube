<?php

namespace App\Components\Search\Exception;

class NotFoundException extends \Exception
{
    protected $code = 404;

    public function __construct($message)
    {
        $this->message = $message;
    }
}

