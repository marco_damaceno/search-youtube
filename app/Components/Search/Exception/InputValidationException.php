<?php

namespace App\Components\Search\Exception;

class InputValidationException extends \Exception
{
    protected $code = 400;

    public function __construct($message, $details = [])
    {
        $this->message = $message;
        $this->details = $details;
    }

    public function getDetails()
    {
        return $this->details;
    }
}

